/*
 *	Copyright(C)2007 by Andreas Bank, andreas_bank@yahoo.se
 *
 *	I won't explain the basic Windows code cause its
 *	not the purpouse of this example
 */
#include "stdafx.h"
#include "d3d.h"
#pragma comment (lib, "d3d9.lib")
#if _DEBUG
	#pragma comment (lib, "d3dx9d.lib")
#else
	#pragma comment (lib, "d3dx9.lib")
#endif

#define SCREEN_WIDTH 1024	//	screen width in pixels, defined for easy changing
#define SCREEN_HEIGHT 768	//	screen height in pixels
#define CUSTOMFVF (D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_DIFFUSE | D3DFVF_TEX1)	//	we skipp writing all flags and makes it easy to change

HWND hParentWnd;
LPDIRECT3DDEVICE9 d3d9dev;	//	the D3D device, contains the scene and all its properties
LPDIRECT3D9 d3d9;	//	D3D interface, turns d3d9dev into an active device
LPDIRECT3DVERTEXBUFFER9 d3d9VertexBuffer1 = NULL;	//	3D object (of vertex arrays and more) buffer in VRAM
LPDIRECT3DVERTEXBUFFER9 d3d9VertexBuffer2 = NULL;	//	same shit as above
LPD3DXMESH mesh1 = NULL, mesh2 = NULL, mesh3 = NULL;	//	predefined simple meshes and .x meshes
DWORD numMaterials = 0;	//	number of mesh (.x) materials (for extraction purpouses)
D3DMATERIAL9 *materialX;	//	array of extracted materials from .x mesh
D3DMATERIAL9 *material;
LPDIRECT3DTEXTURE9 *planeTexture;	//	mesh textures
LPDIRECT3DTEXTURE9 texture1, texture2, texture3, spriteTexture, legendTexture;	//	textures in VRAM
LPD3DXSPRITE sprite = NULL, legendSprite = NULL;	//	sprite
int frame = 8, xpos = 1, ypos = 1, frameGoCount = 127;	//	sprite frame and RECT-bla-bla coords
BOOL frameGo = FALSE;
int backgroundColorRed = 125;	//	background colors, made globals so they can be altered ingame
int backgroundColorGreen = 125;	//	^^
int backgroundColorBlue = 125;	//	^^
float scaleA = 0.5f;	//	scale factor, made global for altering ingame too
float rotateAX = 0.3f;	//	rotate factors, ^^
float rotateAY = 3.7f;
float rotateAZ = 0.0f;
float rotateCamY = 0.0f, moveCamX = 0.0f, moveCamZ = 10.0f, moveCamY = 0.0f;
POINT currentMousePosition, lastMousePosition;
BOOL rotateFromMouse = FALSE;
bool launch = false;	//	I use this one to activate a object launch-animation procedure in renderD3D()
LRESULT CALLBACK WindowProc(HWND, UINT, WPARAM, LPARAM);	//	window event handler function
BOOL initD3d(HWND, BOOL);	//	initialises all D3D interfaces, devices, porperties and other stuff
void renderD3d(void);	//	this is the busiest function that moves, scales, rotates and finally renders the 3D scene
void cleanD3d(void);	//	releases all D3D memory and activated D3D stuff we dirtectly or indirectly used
BOOL initD3dStuff(HWND);	//	initialises (creates) the 3D objects used
void initLightAndMaterial(void);	//	initialises (creates) the lights and objects materials used
struct CUSTOMVERTEX {
	//	this is the structure of the 3D objects we will create adn use
	float X, Y, Z;	//	3D coordinates
	D3DVECTOR normal;	//	vectors, their names are "normals", they decide the lightning on objects
	DWORD color;
	float U, V;	//	texture coordinates
};
int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPTSTR lpCmdLine, int nCmdShow) {
	MSG msg;
	HWND hWnd;
	WNDCLASSEX wcex;
	BOOL fullscreenMode = FALSE;

	ZeroMemory(&msg, sizeof(MSG));
	ZeroMemory(&wcex, sizeof(WNDCLASSEX));

	fullscreenMode = (MessageBox(NULL, "Fullscreen?", "Video mode", MB_YESNO) == IDYES ? 1 : 0);

	wcex.cbSize			= sizeof(WNDCLASSEX); 
	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= (WNDPROC)WindowProc;
	wcex.hInstance		= hInstance;
	wcex.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wcex.hIcon			= LoadIcon(hInstance, (LPCTSTR)IDI_D3D);
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.lpszClassName	= "windowClass";
	wcex.hIconSm		= LoadIcon(wcex.hInstance, (LPCTSTR)IDI_SMALL);

	RegisterClassEx(&wcex);
	if (fullscreenMode) {
		hWnd = CreateWindowEx(NULL, "windowClass", "DX9 - fullscreen", WS_EX_TOPMOST | WS_POPUP, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, NULL, NULL, hInstance, NULL);
	}
	else {
		hWnd = CreateWindowEx(NULL, "windowClass", "DX9 - windowed", WS_OVERLAPPEDWINDOW, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, NULL, NULL, hInstance, NULL);
	}
	hParentWnd = hWnd;
	ShowWindow(hWnd, nCmdShow);
    UpdateWindow(hWnd);

	if (!initD3d(hWnd, fullscreenMode)) return 0;

	while (msg.message != WM_QUIT) {
		DWORD tickCount = GetTickCount();
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		renderD3d();
		while ((GetTickCount() - tickCount) < 25);
	}
	cleanD3d();
	return (int) msg.wParam;
}

LRESULT CALLBACK WindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {
	switch (message) {
	case WM_KEYDOWN:
		switch(wParam) {
			case VK_F1:
				if (backgroundColorRed < 255)
					backgroundColorRed+=10;
				if (backgroundColorRed > 255) backgroundColorRed = 255;
				break;
			case VK_F2:
				if (backgroundColorRed > 0)
					backgroundColorRed-=10;
				if (backgroundColorRed < 0) backgroundColorRed = 0;
				break;
			case VK_F3:
				if (backgroundColorGreen < 255)
					backgroundColorGreen+=10;
				if (backgroundColorGreen > 255) backgroundColorGreen = 255;
				break;
			case VK_F4:
				if (backgroundColorGreen > 0)
					backgroundColorGreen-=10;
				if (backgroundColorGreen < 0) backgroundColorGreen = 0;
				break;
			case VK_F5:
				if (backgroundColorBlue < 255)
					backgroundColorBlue+=10;
				if (backgroundColorBlue > 255) backgroundColorBlue = 255;
				break;
			case VK_F6:
				if (backgroundColorBlue > 0)
					backgroundColorBlue-=10;
				if (backgroundColorBlue < 0) backgroundColorBlue = 0;
				break;
			case VK_UP:
				//	calculate the X and Z coordinates to move camera
				moveCamX += sin(rotateCamY) * 0.5f;
				moveCamZ -= sin(D3DX_PI/2 - rotateCamY) * 0.5f;
				break;
			case VK_DOWN:
				moveCamX -= sin(rotateCamY) * 0.5f;
				moveCamZ += sin(D3DX_PI/2 - rotateCamY) * 0.5f;
				break;
			case VK_RIGHT:
				rotateCamY -= 0.1f;
				break;
			case VK_LEFT:
				rotateCamY += 0.1f;
				break;
			case VK_SHIFT:
				moveCamY -= 0.05f;
				break;
			case VK_CONTROL:
				moveCamY += 0.05f;
				break;
			case VK_RETURN:
				launch = true;
				break;
			case 0x59:	// key Y/y
				if (frameGo == TRUE) {
					for (int i=2000; i>=100; i-=100)
						Beep(i, 10);
					PostQuitMessage(0);
				}
				else
					for (int i=1; i<2000; i+=100)
						Beep(i, 10);
				break;
			case 0x4e:	//	key N/n
				if (frameGo == TRUE && frameGoCount > 179)
					frameGoCount = 177;
				else
					for (int i=1; i<2000; i+=100)
						Beep(i, 10);
				break;
			case VK_ESCAPE:
				frame = 0;
				frameGo = TRUE;
				frameGoCount = 360;
				break;
			default:
				for (int i=1; i<2000; i+=100)
					Beep(i, 10);
				break;
		}
		break;
	case WM_LBUTTONDOWN:
		currentMousePosition.x = LOWORD(lParam);
		currentMousePosition.y = HIWORD(lParam);
		rotateFromMouse = TRUE;
		break;
	case WM_LBUTTONUP:
		rotateFromMouse = FALSE;
		break;
	case WM_MOUSEMOVE:
		currentMousePosition.x = LOWORD(lParam);
		currentMousePosition.y = HIWORD(lParam);
		if (rotateFromMouse) {
			rotateAY -= (float)(currentMousePosition.x - lastMousePosition.x)/180;
			rotateAX -= (float)(currentMousePosition.y - lastMousePosition.y)/180;
		}		
		lastMousePosition.x = currentMousePosition.x;
		lastMousePosition.y = currentMousePosition.y;
		break;
	case WM_CLOSE:
		frame = 0;
		frameGo = TRUE;
		frameGoCount = 360;	//	40 ticks/s, tick: frameGoCount -= 3; (-120 per sec), 360 = 3sec
		break;
	case WM_ENDSESSION:
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}
BOOL initD3d(HWND hWnd, BOOL fullscreenMode) {
	D3DPRESENT_PARAMETERS d3d9pp;

	d3d9 = Direct3DCreate9(D3D_SDK_VERSION);	//	create the Direct3D interface

	ZeroMemory(&d3d9pp, sizeof(D3DPRESENT_PARAMETERS));
	
	if (fullscreenMode)
		d3d9pp.Windowed = FALSE;
	else
		d3d9pp.Windowed = TRUE;

	//	use 64bit display mode if supported by Gcard, else use 32bit or 16bit
	if (SUCCEEDED(d3d9->CheckDeviceType(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, D3DFMT_A16B16G16R16, D3DFMT_A16B16G16R16, FALSE))) {
		d3d9pp.BackBufferFormat = D3DFMT_A16B16G16R16;
		MessageBox(hWnd, "Using 64bit color mode (D3DFMT_A16B16G16R16)", "Information", MB_ICONINFORMATION);
	}
	else if (SUCCEEDED(d3d9->CheckDeviceType(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, D3DFMT_X8R8G8B8, D3DFMT_X8R8G8B8, FALSE)))
		d3d9pp.BackBufferFormat = D3DFMT_X8R8G8B8;
	else if (SUCCEEDED(d3d9->CheckDeviceType(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, D3DFMT_R5G6B5, D3DFMT_R5G6B5, FALSE)))
		d3d9pp.BackBufferFormat = D3DFMT_R5G6B5;
	else if (SUCCEEDED(d3d9->CheckDeviceType(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, D3DFMT_X1R5G5B5, D3DFMT_X1R5G5B5, FALSE)))
		d3d9pp.BackBufferFormat = D3DFMT_X1R5G5B5;
	d3d9pp.BackBufferHeight = SCREEN_HEIGHT;
	d3d9pp.BackBufferWidth = SCREEN_WIDTH;
	d3d9pp.hDeviceWindow = hWnd;
	d3d9pp.SwapEffect = D3DSWAPEFFECT_DISCARD;
	d3d9pp.EnableAutoDepthStencil = TRUE;	//	automatically run the z-buffer for us
	d3d9pp.AutoDepthStencilFormat = D3DFMT_D16;	//	16-bit pixel format for the z-buffer
	d3d9->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWnd, D3DCREATE_SOFTWARE_VERTEXPROCESSING, &d3d9pp, &d3d9dev);
	
	if (!initD3dStuff(hWnd)) return FALSE;	//	init the 3D objects

	initLightAndMaterial();	//	init the lights and objects materials

	d3d9dev->SetRenderState(D3DRS_LIGHTING, D3DZB_TRUE);    //	turn off the 3D lighting
	d3d9dev->SetRenderState(D3DRS_ZENABLE, TRUE);    //	turn on the z-buffer
	d3d9dev->SetRenderState(D3DRS_AMBIENT, D3DCOLOR_XRGB(50, 50, 50));	//	ambient light
	d3d9dev->SetRenderState(D3DRS_NORMALIZENORMALS, TRUE);	//	handle normals in scaling
	//	cant get transperacy to work
	d3d9dev->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
	d3d9dev->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	d3d9dev->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
	d3d9dev->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
	d3d9dev->SetRenderState(D3DRS_MULTISAMPLEANTIALIAS , TRUE);	//	antialising
	//d3d9dev->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_ANISOTROPIC);	//	antistrophic when scaled up
	//d3d9dev->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_ANISOTROPIC);	//	antistrophic when scaled down
	return TRUE;
}
void renderD3d(void) {
	static float movementA = 0.1f, rotateB = 0.0f;
	rotateB += 0.1f;
	rotateAZ = rotateB;
	D3DXMATRIX matrixMoveA, matrixMoveB, matrixMoveC, matrixRotateAX, matrixRotateAX2, matrixRotateAY;
	D3DXMATRIX matrixRotateAY2, matrixRotateAZ, matrixRotateBX, matrixRotateBY, matrixRotateBWide;
	D3DXMATRIX matrixRotateMesh3, matrixScaleA, matrixScaleB, matrixView, matrixProjection;	//	pipeline projection matrixes
	D3DXMATRIX matrixCamRotateY, matrixCamMove;
	RECT part;	//	rectangular area for sprite drawing
	D3DXVECTOR3 center(0.0f, 0.0f, 0.0f);	//	center at the upper-left corner (I dont get it) for sprite
	D3DXVECTOR3 position((float)SCREEN_WIDTH/2-128, 20.0f, 0.0f);	//	position sprite at center
	if (launch)
		if (movementA < 50.0f) movementA *= 1.1f;
		else movementA += 5.0f;
		if (movementA > 100.0f) {
			movementA = 0.1f;
			launch = false;
		}

	// clear the window to specified colors
	d3d9dev->Clear(0, NULL, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, D3DCOLOR_XRGB(backgroundColorRed, backgroundColorGreen, backgroundColorBlue), 1.0f, 0);

	//	pipeline transformations
	//	build a matrix to rotate the model based on the increasing float value
	// do 3D rendering on the back buffer here
	d3d9dev->BeginScene();    // begins the 3D scene
	D3DXMatrixLookAtLH(&matrixView,
						&D3DXVECTOR3 (0.0f, 0.0f, 10.0f),    // the camera position
						&D3DXVECTOR3 (0.0f, 0.0f, 0.0f),    // the look-at position
						&D3DXVECTOR3 (0.0f, 1.0f, 0.0f));    // the up direction
	d3d9dev->SetTransform(D3DTS_VIEW, &matrixView);    // set the view transform to matView
	D3DXMatrixPerspectiveFovLH(&matrixProjection,
								D3DXToRadian(45),    // the horizontal field of view
								(FLOAT)SCREEN_WIDTH / (FLOAT)SCREEN_HEIGHT, // aspect ratio
								1.0f,    //	the near view-plane
								100.0f);    // the far view-plane
	d3d9dev->SetTransform(D3DTS_PROJECTION, &matrixProjection);    // set the projection

	D3DXMatrixTranslation(&matrixCamMove, moveCamX, moveCamY, moveCamZ);
	D3DXMatrixRotationY(&matrixCamRotateY, rotateCamY);
	d3d9dev->SetTransform(D3DTS_VIEW, &(matrixCamMove*matrixCamRotateY));	//	move camera

	d3d9dev->SetFVF(CUSTOMFVF);	// select which vertex format we are using
	d3d9dev->SetStreamSource(0, d3d9VertexBuffer1, 0, sizeof(CUSTOMVERTEX));	// select the triangle vertex buffer to display
	d3d9dev->SetMaterial(material);
	d3d9dev->SetTexture(0, texture3);
	D3DXMatrixTranslation(&matrixMoveA, -2.0f, 2.0f, 3.0f);
	D3DXMatrixRotationX(&matrixRotateAX, rotateAX);
	D3DXMatrixRotationX(&matrixRotateAX2, rotateAX-2.0f*rotateAX);
	D3DXMatrixRotationY(&matrixRotateAY, rotateAY);
	D3DXMatrixRotationY(&matrixRotateAY2, rotateAY+3.14159f);
	D3DXMatrixRotationZ(&matrixRotateAZ, rotateAZ);
	//	a matrix to scale the triangle
	D3DXMatrixScaling(&matrixScaleA, scaleA, scaleA, scaleA);
	//	tell Direct3D about our matrixes, set them in action
	d3d9dev->SetTransform(D3DTS_WORLD, &(matrixRotateAX*matrixRotateAY*matrixRotateAZ*matrixMoveA));
	d3d9dev->DrawPrimitive(D3DPT_TRIANGLELIST, 0, 1);
	d3d9dev->SetTransform(D3DTS_WORLD, &(matrixRotateAX2*matrixRotateAY2*matrixRotateAZ*matrixMoveA));
	d3d9dev->DrawPrimitive(D3DPT_TRIANGLELIST, 0, 1);

	//	draw mesh, using almost same transformations as the triangle
	D3DXMatrixRotationY(&matrixRotateMesh3, 1.0f);
	D3DXMatrixTranslation(&matrixMoveA, 0.0f, 0.0f, (0.0f-movementA));
	d3d9dev->SetTransform(D3DTS_WORLD, &(matrixScaleA*matrixMoveA*matrixRotateAX*matrixRotateAY));
	for (DWORD i = 0; i < numMaterials; i++) {
		d3d9dev->SetMaterial(&materialX[i]);
		d3d9dev->SetTexture(0, planeTexture[i]);
		mesh3->DrawSubset(i);
	}

	d3d9dev->SetFVF(CUSTOMFVF);
	d3d9dev->SetMaterial(material);
	//	cube
	D3DXMatrixTranslation(&matrixMoveB, 0.0f, 0.0f, 3.0f);
	D3DXMatrixRotationX(&matrixRotateBX, rotateB);
	D3DXMatrixRotationY(&matrixRotateBY, rotateB);
	D3DXMatrixRotationY(&matrixRotateBWide, rotateB);
	D3DXMatrixScaling(&matrixScaleB, 0.2f, 0.2f, 0.2f);
	//	set vertex buffer source (contains the vertices coord-array in the video ram)
	d3d9dev->SetStreamSource(0, d3d9VertexBuffer2, 0, sizeof(CUSTOMVERTEX));
	// set the texture to be drawn on everything below
	d3d9dev->SetTexture(0, texture1);
	//	apply transformations (animation matrixes)
	d3d9dev->SetTransform(D3DTS_WORLD, &(matrixRotateBX*matrixRotateBY*matrixScaleB*matrixMoveB*matrixRotateBWide));
	d3d9dev->DrawPrimitive(D3DPT_TRIANGLESTRIP, 0, 2);
	//	change texture for last side of cube
	//	set the texture to be drawn on everything below
	d3d9dev->SetTexture(0, texture2);
	d3d9dev->DrawPrimitive(D3DPT_TRIANGLESTRIP, 4, 2);
	d3d9dev->SetTexture(0, texture1);
	d3d9dev->DrawPrimitive(D3DPT_TRIANGLESTRIP, 8, 2);
	d3d9dev->DrawPrimitive(D3DPT_TRIANGLESTRIP, 12, 2);
	d3d9dev->DrawPrimitive(D3DPT_TRIANGLESTRIP, 16, 2);
	d3d9dev->DrawPrimitive(D3DPT_TRIANGLESTRIP, 20, 2);

	//	cube2
	D3DXMatrixTranslation(&matrixMoveB, 2.0f, 2.0f, 3.0f);
	D3DXMatrixRotationX(&matrixRotateBX, rotateB*1.1f);
	D3DXMatrixRotationY(&matrixRotateBY, rotateB*0.9f);
	d3d9dev->SetTransform(D3DTS_WORLD, &(matrixRotateBX*matrixRotateBY*matrixScaleB*matrixMoveB));
	d3d9dev->DrawPrimitive(D3DPT_TRIANGLESTRIP, 0, 2);
	//	change texture for last side of cube
	//	set the texture to be drawn on everything below
	d3d9dev->SetTexture(0, texture2);
	d3d9dev->DrawPrimitive(D3DPT_TRIANGLESTRIP, 4, 2);
	d3d9dev->SetTexture(0, texture1);
	d3d9dev->DrawPrimitive(D3DPT_TRIANGLESTRIP, 8, 2);
	d3d9dev->DrawPrimitive(D3DPT_TRIANGLESTRIP, 12, 2);
	d3d9dev->DrawPrimitive(D3DPT_TRIANGLESTRIP, 16, 2);
	d3d9dev->DrawPrimitive(D3DPT_TRIANGLESTRIP, 20, 2);

	//	cube3
	D3DXMatrixTranslation(&matrixMoveB, -2.0f, -2.0f, 3.0f);
	D3DXMatrixRotationX(&matrixRotateBX, rotateB*0.44f);
	D3DXMatrixRotationY(&matrixRotateBY, rotateB);
	d3d9dev->SetTransform(D3DTS_WORLD, &(matrixRotateBX*matrixRotateBY*matrixScaleB*matrixMoveB));
	d3d9dev->DrawPrimitive(D3DPT_TRIANGLESTRIP, 0, 2);
	//	change texture for last side of cube
	//	set the texture to be drawn on everything below
	d3d9dev->SetTexture(0, texture2);
	d3d9dev->DrawPrimitive(D3DPT_TRIANGLESTRIP, 4, 2);
	d3d9dev->SetTexture(0, texture1);
	d3d9dev->DrawPrimitive(D3DPT_TRIANGLESTRIP, 8, 2);
	d3d9dev->DrawPrimitive(D3DPT_TRIANGLESTRIP, 12, 2);
	d3d9dev->DrawPrimitive(D3DPT_TRIANGLESTRIP, 16, 2);
	d3d9dev->DrawPrimitive(D3DPT_TRIANGLESTRIP, 20, 2);

	d3d9dev->SetTexture(0, NULL);
	D3DXMatrixTranslation(&matrixMoveB, 5.0f, -5.0f, -20.0f);
	d3d9dev->SetTransform(D3DTS_WORLD, &(matrixRotateAY*matrixRotateAX*matrixMoveB));
	mesh1->DrawSubset(0);

	//	spheres
	D3DXMatrixRotationX(&matrixRotateBX, rotateB);
	D3DXMatrixRotationY(&matrixRotateBY, rotateB*0.9f);
	D3DXMatrixRotationZ(&matrixRotateAZ, rotateB*1.1f);
	D3DXMatrixTranslation(&matrixMoveA, -1.4f, 0.0f, 0.0f);
	D3DXMatrixTranslation(&matrixMoveB, -2.0f, -2.0f, 3.0f);
	d3d9dev->SetTransform(D3DTS_WORLD, &(matrixMoveA*matrixRotateBY*matrixRotateBX*matrixRotateAZ*matrixMoveB));
	mesh2->DrawSubset(0);
	D3DXMatrixTranslation(&matrixMoveA, 1.4f, 0.0f, 0.0f);
	D3DXMatrixTranslation(&matrixMoveB, -2.0f, -2.0f, 3.0f);
	d3d9dev->SetTransform(D3DTS_WORLD, &(matrixMoveA*matrixRotateBY*matrixRotateBX*matrixMoveB));
	mesh2->DrawSubset(0);

	//	sprite legend
	legendSprite->Begin(D3DXSPRITE_ALPHABLEND);
		SetRect(&part, 0, 0, 256, 256);
		position = D3DXVECTOR3(0.0f, 0.0f, 0.0f);	//	position at center
		legendSprite->Draw(legendTexture, &part, &center, &position, D3DCOLOR_ARGB(127, 255, 255, 255));
    legendSprite->End();	//	end sprite drawing

	//	sprite menu effect
	sprite->Begin(D3DXSPRITE_ALPHABLEND);

		/*	sprite sample by directxtutorial.com
		//	count from 0 to 22 to determine the current frame
		if(frame < 22) frame++;	//	if we aren't on the last frame, go to the next frame
		if(frame < 11) xpos = frame * 182 + 1;
		if(frame > 12 && frame < 22) xpos = (frame - 12) * 182 + 1;
		if(frame < 11) ypos = 1;
		if(frame > 12 && frame < 22) ypos = 123;

		//	draw the selected frame using the coordinates
		SetRect(&part, xpos, ypos, xpos + 181, ypos + 121);
		position = D3DXVECTOR3((float)SCREEN_WIDTH/2-91, (float)SCREEN_HEIGHT/2-61, 0.0f);	//	position at center
		*/
	
		//	my sprite
		//	sprite.png frames:
		//	1st row    1 | 2 | 3 | 4
		//	           -------------
		//	2nd row    5 | 6 | 7 | 8
		if (frame < 8) {
			frame++;	//	if we aren't on the last frame, go to the next frame
		}
		if (frame == 1) {
			xpos = 1;	//	if we're on the first frame on the first row, xpos is at the first horizontal pixel
		}
		else if (frame < 5) {
			xpos = (frame - 1) * 513;	//	if we're on the first row of the sprite
		}
		else if (frame > 4) {
			xpos = (frame - 5) * 513;	//	if we're at the second row of the sprite
		}
		if (frame < 5) {
			ypos = 1;	//	if were on the first row, ypos is at the first vertical pixel
		}
		else {
			ypos = 257;	//	if we're on the second row, ypos starts from hte 257th pixel (1-256 are in the first row)
		}
		SetRect(&part, xpos, ypos, xpos + 511, ypos + 255);	//	construct the selected frame using the coordinates
		position = D3DXVECTOR3((float)SCREEN_WIDTH/2-256, (float)SCREEN_HEIGHT/2-128, 0.0f);	//	position at center
		if (frameGo) {	//	this adds transparent dissapearing of the sprite after ~5s
			sprite->Draw(spriteTexture, &part, &center, &position, D3DCOLOR_ARGB(((frameGoCount>180)?180:frameGoCount), 255, 255, 255));
			frameGoCount -= 3;
			if (frameGoCount < 5) {
				frameGo = FALSE;
			}
		}
    sprite->End();	//	end sprite drawing

	d3d9dev->EndScene();	//	ends the 3D scene
	d3d9dev->Present(NULL, NULL, NULL, NULL);	//	displays the created frame on the screen
	return;
}
void cleanD3d(void) {
	d3d9VertexBuffer1->Release();
	d3d9VertexBuffer2->Release();
	if (mesh1 != NULL) mesh1->Release();
	if (mesh2 != NULL) mesh2->Release();
	if (mesh3 != NULL) mesh3->Release();
	if (texture1 != NULL) texture1->Release();
	if (texture2 != NULL) texture2->Release();
	if (texture3 != NULL) texture3->Release();
	if (sprite != NULL) sprite->Release();
	if (spriteTexture != NULL) spriteTexture->Release();
	if (legendSprite != NULL) legendSprite->Release();
	if (legendTexture != NULL) legendTexture->Release();
	for (DWORD i = 0; i < numMaterials; i++)
		//	customized cause I use textures from resources
		if ((i != 1) && (i != 3) && (i != 4) && (i != 6) && planeTexture[i] != NULL)
			planeTexture[i]->Release();
	d3d9dev->Release();
	d3d9->Release();
	return;
}

BOOL initD3dStuff(HWND hWnd) {
	void *tempVoid;
	LPD3DXBUFFER shipMB;
	D3DXMATERIAL *tempMaterials;
	int transparency = 127;

	CUSTOMVERTEX customVertex1[] = {
		//	triangle
		{0.8f, -0.8f, 0.0f, 0, 0, 1, D3DCOLOR_ARGB(255, 255, 255, 255), 0, 0},
		{0.0f, 0.8f, 0.0f, 0, 0, 1, D3DCOLOR_ARGB(255, 255, 255, 255), 1, 0},
		{-0.8f, -0.8f, 0.0f, 0, 0, 1, D3DCOLOR_ARGB(255, 255, 255, 255), 1, 1}
	};
	CUSTOMVERTEX customVertex2[] = {
		//	side 1
		{-3.0f, 3.0f, -3.0f, 0, 0, -1, D3DCOLOR_ARGB(transparency, 255, 255, 255), 0, 0},
		{3.0f, 3.0f, -3.0f, 0, 0, -1, D3DCOLOR_ARGB(transparency, 255, 255, 255), 0, 1},
		{-3.0f, -3.0f, -3.0f, 0, 0, -1, D3DCOLOR_ARGB(transparency, 255, 255, 255), 1, 0},
		{3.0f, -3.0f, -3.0f, 0, 0, -1, D3DCOLOR_ARGB(transparency, 255, 255, 255), 1, 1},

		//	side 2
		{-3.0f, 3.0f, 3.0f, 0, 0, 1, D3DCOLOR_ARGB(transparency, 255, 255, 255), 0, 0},
		{-3.0f, -3.0f, 3.0f, 0, 0, 1, D3DCOLOR_ARGB(transparency, 255, 255, 255), 0, 1},
		{3.0f, 3.0f, 3.0f, 0, 0, 1, D3DCOLOR_ARGB(transparency, 255, 255, 255), 1, 0},
		{3.0f, -3.0f, 3.0f, 0, 0, 1, D3DCOLOR_ARGB(transparency, 255, 255, 255), 1, 1},

		//	side 3
		{-3.0f, 3.0f, 3.0f, 0, 1, 0, D3DCOLOR_ARGB(transparency, 255, 255, 255), 0, 0},
		{3.0f, 3.0f, 3.0f, 0, 1, 0, D3DCOLOR_ARGB(transparency, 255, 255, 255), 0, 1},
		{-3.0f, 3.0f, -3.0f, 0, 1, 0, D3DCOLOR_ARGB(transparency, 255, 255, 255), 1, 0},
		{3.0f, 3.0f, -3.0f, 0, 1, 0, D3DCOLOR_ARGB(transparency, 255, 255, 255), 1, 1},

		//	side 4
		{-3.0f, -3.0f, 3.0f, 0, -1, 0, D3DCOLOR_ARGB(transparency, 255, 255, 255), 0, 0},
		{-3.0f, -3.0f, -3.0f, 0, -1, 0, D3DCOLOR_ARGB(transparency, 255, 255, 255), 0, 1},
		{3.0f, -3.0f, 3.0f, 0, -1, 0, D3DCOLOR_ARGB(transparency, 255, 255, 255), 1, 0},
		{3.0f, -3.0f, -3.0f, 0, -1, 0, D3DCOLOR_ARGB(transparency, 255, 255, 255), 1, 1},

		//	side 5
		{3.0f, 3.0f, -3.0f, 1, 0, 0, D3DCOLOR_ARGB(transparency, 255, 255, 255), 0, 0},
		{3.0f, 3.0f, 3.0f, 1, 0, 0, D3DCOLOR_ARGB(transparency, 255, 255, 255), 0, 1},
		{3.0f, -3.0f, -3.0f, 1, 0, 0, D3DCOLOR_ARGB(transparency, 255, 255, 255), 1, 0},
		{3.0f, -3.0f, 3.0f, 1, 0, 0, D3DCOLOR_ARGB(transparency, 255, 255, 255), 1, 1},

		//	side 6
		//	I inverted the texture coords to get my image right
		{-3.0f, 3.0f, -3.0f, -1, 0, 0, D3DCOLOR_ARGB(transparency, 255, 255, 255), 0, 0},
		{-3.0f, -3.0f, -3.0f, -1, 0, 0, D3DCOLOR_ARGB(transparency, 255, 255, 255), 0, 1},
		{-3.0f, 3.0f, 3.0f, -1, 0, 0, D3DCOLOR_ARGB(transparency, 255, 255, 255), 1, 0},
		{-3.0f, -3.0f, 3.0f, -1, 0, 0, D3DCOLOR_ARGB(transparency, 255, 255, 255), 1, 1}
	};

	//	predefined simple D3DX mesh objects
	D3DXCreateBox(d3d9dev, 7.0f, 3.0f, 3.0f, &mesh1, NULL);
	D3DXCreateSphere(d3d9dev, 0.3f, 20, 10, &mesh2, NULL);
	
	//	one complete mesh loading (vertices, materials and textures)
	if (D3DXLoadMeshFromXResource(NULL, (LPCSTR)MAKEINTRESOURCE(IDR_MESHX1), "meshx", D3DXMESH_MANAGED, d3d9dev, NULL, &shipMB, NULL, &numMaterials, &mesh3) != D3D_OK) {
		MessageBox(hWnd, "craft.x could not be loaded", "Error", MB_ICONERROR);
		PostQuitMessage(0);
		return FALSE;
	}
	/*	load from file
	if (D3DXLoadMeshFromX("craft.x", D3DXMESH_MANAGED, d3d9dev, NULL, &shipMB, NULL, &numMaterials, &mesh3) != D3D_OK) {
		MessageBox(hWnd, "craft.x file not found", "Error", MB_ICONERROR);
		PostQuitMessage(0);
	}
	*/
	tempMaterials = (D3DXMATERIAL *)shipMB->GetBufferPointer();
	materialX = new D3DMATERIAL9[numMaterials];
	planeTexture = new LPDIRECT3DTEXTURE9[numMaterials];
	for (DWORD i = 0; i < numMaterials; i++) {
		materialX[i] = tempMaterials[i].MatD3D;
		materialX[i].Ambient = materialX[i].Diffuse;
/*	dont need the following cause I use resources
		USES_CONVERSION;    // allows certain string conversions
		// if there is a texture to load, load it
		if(FAILED(D3DXCreateTextureFromFile(d3d9dev, CA2W(tempMaterials[i].pTextureFilename), &planeTexture[i])))
*/			planeTexture[i] = NULL;	// if there is no texture, set the texture to NULL

	}

	// load the textures (embedded in the executable) we will use
	if (D3DXCreateTextureFromResource(d3d9dev, NULL, (LPCSTR)MAKEINTRESOURCE(IDB_BITMAP1), &planeTexture[2]) != D3D_OK) {
		MessageBox(hWnd, "wings.bmp could not be loaded", "Error", MB_ICONERROR);
		PostQuitMessage(0);
		return FALSE;
	}
	if (D3DXCreateTextureFromResource(d3d9dev, NULL, (LPCSTR)MAKEINTRESOURCE(IDB_BITMAP2), &planeTexture[6]) != D3D_OK) {
		MessageBox(hWnd, "bihull.bmp could not be loaded", "Error", MB_ICONERROR);
		PostQuitMessage(0);
		return FALSE;
	}
	planeTexture[0] = planeTexture[1] = planeTexture[3] = planeTexture[4] = planeTexture[6];

	if (D3DXCreateTextureFromResourceEx(d3d9dev,	//	the device pointer
								NULL,	//	module (executable containing the resource)
								(LPCSTR)MAKEINTRESOURCE(IDB_BITMAP3),	//	resource
								//"logo1.bmp",	//	the file name
								D3DX_DEFAULT,	//	default width
								D3DX_DEFAULT,	//	default height
								D3DX_DEFAULT,	//	no mip mapping
								NULL,	//	regular usage
								D3DFMT_A8R8G8B8,	//	32-bit pixels with alpha
								D3DPOOL_MANAGED,	//	typical memory handling
								D3DX_DEFAULT,	//	no filtering
								D3DX_DEFAULT,	//	no mip filtering
								D3DCOLOR_ARGB(127, 255, 0, 255),	//	the hot-pink color key
								NULL,	//	no image info struct
								NULL,	//	not using 256 colors
								&texture1) != D3D_OK) {
		MessageBox(hWnd, "logo1.bmp could not be loaded", "Error", MB_ICONERROR);
		PostQuitMessage(0);
		return FALSE;
	}
/*	if (D3DXCreateTextureFromResource(d3d9dev, NULL, (LPCSTR)MAKEINTRESOURCE(IDB_BITMAP3), &texture1) != D3D_OK) {
		MessageBox(hWnd, "logo1.bmp could not be loaded", "Error", MB_ICONERROR);
		PostQuitMessage(0);
		return FALSE;
	}
*/

	if (D3DXCreateTextureFromResourceEx(d3d9dev,	//	the device pointer
								NULL,	//	module (executable containing the resource)
								(LPCSTR)MAKEINTRESOURCE(IDR_LOGO2PNG),	//	resource
								//"logo2.png",	//	the file name
								D3DX_DEFAULT,	//	default width
								D3DX_DEFAULT,	//	default height
								D3DX_DEFAULT,	//	no mip mapping
								NULL,	//	regular usage
								D3DFMT_A8R8G8B8,	//	32-bit pixels with alpha
								D3DPOOL_MANAGED,	//	typical memory handling
								D3DX_DEFAULT,	//	no filtering
								D3DX_DEFAULT,	//	no mip filtering
								D3DCOLOR_ARGB(127, 255, 0, 255),	//	the hot-pink color key
								NULL,	//	no image info struct
								NULL,	//	not using 256 colors
								&texture2) != D3D_OK) {
		MessageBox(hWnd, "logo2.png could not be loaded", "Error", MB_ICONERROR);
		PostQuitMessage(0);
		return FALSE;
	}
/*
	if (D3DXCreateTextureFromResource(d3d9dev, NULL, (LPCSTR)MAKEINTRESOURCE(IDR_LOGO2PNG), &texture2) != D3D_OK) {
		MessageBox(hWnd, "logo2.png could not be loaded", "Error", MB_ICONERROR);
		PostQuitMessage(0);
		return FALSE;
	}
*/
	if (D3DXCreateTextureFromResourceEx(d3d9dev,	//	the device pointer
								NULL,	//	module (executable containing the resource)
								(LPCSTR)MAKEINTRESOURCE(IDR_LOGO3PNG),	//	resource
								//"logo3.png",	//	the file name
								D3DX_DEFAULT,	//	default width
								D3DX_DEFAULT,	//	default height
								D3DX_DEFAULT,	//	no mip mapping
								NULL,	//	regular usage
								D3DFMT_A8R8G8B8,	//	32-bit pixels with alpha
								D3DPOOL_MANAGED,	//	typical memory handling
								D3DX_DEFAULT,	//	no filtering
								D3DX_DEFAULT,	//	no mip filtering
								D3DCOLOR_ARGB(127, 255, 0, 255),	//	the hot-pink color key
								NULL,	//	no image info struct
								NULL,	//	not using 256 colors
								&texture3) != D3D_OK) {
		MessageBox(hWnd, "logo3.png could not be loaded", "Error", MB_ICONERROR);
		PostQuitMessage(0);
		return FALSE;
	}
/*	if (D3DXCreateTextureFromResource(d3d9dev, NULL, (LPCSTR)MAKEINTRESOURCE(IDR_LOGO3PNG), &texture3) != D3D_OK) {
		MessageBox(hWnd, "logo3.png could not be loaded", "Error", MB_ICONERROR);
		PostQuitMessage(0);
		return FALSE;
	}
*/

	//	sprite
	D3DXCreateSprite(d3d9dev, &sprite);
	// sprite texture
	if (D3DXCreateTextureFromResourceEx(d3d9dev,	//	the device pointer
								NULL,	//	module (executable containing the resource)
								(LPCSTR)MAKEINTRESOURCE(IDR_LEGENDPNG),	//	resource
								//"legend.png",	//	the file name
								D3DX_DEFAULT,	//	default width
								D3DX_DEFAULT,	//	default height
								D3DX_DEFAULT,	//	no mip mapping
								NULL,	//	regular usage
								D3DFMT_A8R8G8B8,	//	32-bit pixels with alpha
								D3DPOOL_MANAGED,	//	typical memory handling
								D3DX_DEFAULT,	//	no filtering
								D3DX_DEFAULT,	//	no mip filtering
								D3DCOLOR_XRGB(255, 0, 255),	//	the hot-pink color key
								NULL,	//	no image info struct
								NULL,	//	not using 256 colors
								&legendTexture) != D3D_OK) {
		MessageBox(hWnd, "legend.png could not be loaded", "Error", MB_ICONERROR);
		PostQuitMessage(0);
		return FALSE;
	}

	//	sprite legend
	D3DXCreateSprite(d3d9dev, &legendSprite);
	//	its texture
	if (D3DXCreateTextureFromResourceEx(d3d9dev,	//	the device pointer
								NULL,
								(LPCSTR)MAKEINTRESOURCE(IDR_SPRITEPNG),
								//"sprite.png",	//	the file name
								D3DX_DEFAULT,	//	default width
								D3DX_DEFAULT,	//	default height
								D3DX_DEFAULT,	//	no mip mapping
								NULL,	//	regular usage
								D3DFMT_A8R8G8B8,	//	32-bit pixels with alpha
								D3DPOOL_MANAGED,	//	typical memory handling
								D3DX_DEFAULT,	//	no filtering
								D3DX_DEFAULT,	//	no mip filtering
								D3DCOLOR_XRGB(255, 0, 255),	//	the hot-pink color key
								NULL,	//	no image info struct
								NULL,	//	not using 256 colors
								&spriteTexture) != D3D_OK) {
		MessageBox(hWnd, "panel.png could not be loaded", "Error", MB_ICONERROR);
		PostQuitMessage(0);
		return FALSE;
	}

	//	triangle
	d3d9dev->CreateVertexBuffer(3*sizeof(CUSTOMVERTEX), 0, CUSTOMFVF, D3DPOOL_MANAGED, &d3d9VertexBuffer1, NULL);
	d3d9VertexBuffer1->Lock(0, 0, &tempVoid, 0);
	if (memcpy(tempVoid, customVertex1, sizeof(customVertex1)) == NULL) MessageBox(NULL, "Error", "error", MB_OK);
	d3d9VertexBuffer1->Unlock();

	//	cube
	d3d9dev->CreateVertexBuffer(24*sizeof(CUSTOMVERTEX), 0, CUSTOMFVF, D3DPOOL_MANAGED, &d3d9VertexBuffer2, NULL);
	d3d9VertexBuffer2->Lock(0, 0, &tempVoid, 0);
	memcpy(tempVoid, customVertex2, sizeof(customVertex2));
	d3d9VertexBuffer2->Unlock();

	return TRUE;
}

void initLightAndMaterial(void) {
	D3DLIGHT9 light;
	//	D3DVECTOR vecDirection = {-1.0f, -0.3f, -1.0f};

	ZeroMemory(&light, sizeof(light));
	light.Type = D3DLIGHT_POINT;	//	D3DLIGHT_POINT (lamp), D3DLIGHT_SPOT (ficklampa), D3DLIGHT_DIRECTIONAL (sunlight, no origin)
	light.Diffuse.r = 0.5f;
	light.Diffuse.g = 0.5f;
	light.Diffuse.b = 0.5f;
	light.Diffuse.a = 1.0f;	//	alpha... dunno what its used for
	//	light.Direction = vecDirection;
	light.Position.x = 1.0f;
	light.Position.y = 2.0f;
	light.Position.z = 7.0f;
	light.Range = 60.0f;
	//	the equation shit, Atten(illumination level) = 1 / (att0 + att1 * d + att2 * d^2)
	light.Attenuation0 = 0.0f;	//	constant light
	light.Attenuation1 = 0.125f;	//	far falloff
	light.Attenuation2 = 0.0f;	//	close fallof, close objects are highly lit

	d3d9dev->SetLight(0, &light);	//	set this light to be light Nr.0
	d3d9dev->LightEnable(0, TRUE);	//	enable light Nr.0

	material = (D3DMATERIAL9 *)malloc(sizeof(D3DMATERIAL9));
	ZeroMemory(material, sizeof(D3DMATERIAL9));	//	clear out the struct for use
	material->Diffuse.r = 1.0f;	//	set the material to full red
	material->Diffuse.g = 1.0f;	//	set the material to full green
	material->Diffuse.b = 1.0f;	//	set the material to full blue
	material->Diffuse.a = 0.5f;	//	set the material to full alpha
	material->Ambient.r = 1.0f;	//	set the material to full red
	material->Ambient.g = 1.0f;	//	set the material to full green
	material->Ambient.b = 1.0f;	//	set the material to full blue
	material->Ambient.a = 0.5f;	//	set the material to full alpha

	return;
}