//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by d3d.rc
//
#define IDC_MYICON                      2
#define IDD_D3D_DIALOG                  102
#define IDS_APP_TITLE                   103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_D3D                         107
#define IDI_SMALL                       108
#define IDC_D3D                         109
#define IDR_MAINFRAME                   128
#define IDR_LOGO3PNG                    128
#define IDR_MESHX1                      135
#define IDB_BITMAP1                     136
#define IDB_BITMAP2                     137
#define IDB_BITMAP3                     138
#define IDR_LOGO2PNG                    143
#define IDR_LEGENDPNG                   151
#define IDR_SPRITEPNG                   152
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        154
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
